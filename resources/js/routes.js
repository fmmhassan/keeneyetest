import LandingComponent from './components/LandingComponent';
import PostComponent from './components/post';

export const routes = [
    {
        path : '/',
        name : 'welcome',
        component : LandingComponent,
    },
    {
        path : '/post',
        name : 'post',
        component : PostComponent,
    },
];