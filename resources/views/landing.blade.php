@extends('masters.master')
@section('content')
<div class="content offset-md-2" id="crud_app">
    <div class="container">
        <div class="row">
            <navigation-menu/>
        </div>
        <router-view></router-view>
    </div>
    
</div>
@endsection
@push('scripts')
<script>
</script>
@endpush